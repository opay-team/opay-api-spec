run:
	npm start
install:
	npm install
deploy:
	curl -u 95pnyt4EoJfuluQTrB7ZQkWcP2wIdb28: --request PUT --url https://dash.readme.io/api/v1/swagger/5b4f113eb270b4000386ce97 -F 'swagger=@web_deploy/authorization.json'
	curl -u 95pnyt4EoJfuluQTrB7ZQkWcP2wIdb28: --request PUT --url https://dash.readme.io/api/v1/swagger/5b4f1164d9ea0b00031c6804 -F 'swagger=@web_deploy/gateway.json'
	curl -u 95pnyt4EoJfuluQTrB7ZQkWcP2wIdb28: --request PUT --url https://dash.readme.io/api/v1/swagger/5b4f117339f3100003031054 -F 'swagger=@web_deploy/order.json'
	curl -u 95pnyt4EoJfuluQTrB7ZQkWcP2wIdb28: --request PUT --url https://dash.readme.io/api/v1/swagger/5b5e2dd1efd5c6000322132a -F 'swagger=@web_deploy/remittance.json'
	curl -u 95pnyt4EoJfuluQTrB7ZQkWcP2wIdb28: --request PUT --url https://dash.readme.io/api/v1/swagger/5be473cff11a8800603ae200 -F 'swagger=@web_deploy/users.json'
.PHONY: run
