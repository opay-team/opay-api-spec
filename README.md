# OperaPay API Specification

This repository contains the REST API specification in OpenAPI (formerly known as swagger) format.

*OpenAPI specification* is a community driven effort to standardize API specification 

[Here](https://any-api.com/) is a list of currently available API specification, which includes Amazon , google, facebook , paypal, instagram ,, spotify and many other piublic APIs

[Here](http://openapi.tools/) is a list of tooling around OpenAPI specification (including documentation, editor, mock-server for testing, parsing and ultimately easy discoverability and integration by users) 

# Prerequisite 
You need to have nodejs and npm installed to get the editor and doc generated

# Usage
1. `git clone git@bitbucket.org:opay-team/opay-api-spec.git`
2. `cd opay-api-spec`
3. `npm install && npm start`
This runs the editor on http://localhost:5000 and documentation on http://localhost:3000

In order to generate code from swagger.yaml

1. Install [swagger-codegen] (https://github.com/swagger-api/swagger-codegen#prerequisites)
2. Run `swagger-codegen generate -i spec/swagger.yaml -l go -o /tmp/go` in order to generate go client code 

# Note
This is just work in progress. Currently we have only `gateway/create` included in this spec
This is to demonstrate [schema-first API development](https://nordicapis.com/using-a-schema-first-design-as-your-single-source-of-truth/) of our Rest API

