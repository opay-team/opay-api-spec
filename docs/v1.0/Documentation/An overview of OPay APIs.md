---
title: "An overview of OPay APIs"
excerpt: "This page gives an overview of OPay APIs and high level steps to start integrating with OPay APIs"
---
## Overview
OPay provides easy-to-use checkout integration with OPay Checkout as well as generic and powerful RESTful APIs which allow merchants to solve most of their payment use cases.

The Gateway APIs allow merchants to accept payments from end user into the merchant's OPay wallet.

The Order APIs allow merchants to allow end user to make payments towards utilities or to transfer money to another OPay wallet.

## Use cases
* Accept Payments
  * From end user's card to merchant's wallet
  * From end user's bank account to merchant's wallet
  * From end user's OPay wallet to merchant's wallet
  * [Tokenization support](https://www.3dsi.com/blog/credit-card-tokenization-101) to securely save payment instrument for recurrent payment
  * Supports client-only and client-server deployment on merchant side
  
* Make Payments
  *  From end user's card to utility recipients (airtime/water/electricity/tv/data/rent)
  * [Tokenization support](https://www.3dsi.com/blog/credit-card-tokenization-101) to securely save payment instrument for recurrent payment
  * Peer-to-Peer payment: Remit money from any instrument to recipient opay wallet

## Types of Integration
OPay provides RESTful APIs for a more powerful integration from the merchant's website or mobile application. 

It also includes OPay Checkout, which provides merchant's an easy payment checkout integration for their website or mobile app with simple and guided programming.