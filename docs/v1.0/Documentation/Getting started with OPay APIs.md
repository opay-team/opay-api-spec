---
title: "Getting started with OPay APIs"
excerpt: "This page provides the steps to create the merchant dashboard account"
---
## Create Merchant Dashboard Account
The first step for starting to integrate with OPay APIs would be to create the Merchant Dashboard account in the OPay Sandbox environment [here](https://dashboard.bz-sandbox.opay-test.net/). 

This environment is a replica of your production environment which gives you a feel of all the transaction you will be doing via APIs.

The Dashboard also allows you to retrieve the API keys needed for API Authorization

### Sign Up

Sign up to create a Merchant Dashboard account in the sandbox account using the following URL: https://dashboard.bz-sandbox.opay-test.net
[block:image]
{
  "images": [
    {
      "image": [
        "https://files.readme.io/ef90ee6-Screenshot_from_2018-06-26_17-02-37.png",
        "Screenshot from 2018-06-26 17-02-37.png",
        1717,
        997,
        "#4a4068"
      ]
    }
  ]
}
[/block]
### Log in and select Settings
Once your account is created, you can log in to see the following landing page:
Go to "Settings."
[block:image]
{
  "images": [
    {
      "image": [
        "https://files.readme.io/93fc33c-MerchantDashboard.png",
        "MerchantDashboard.png",
        1717,
        997,
        "#dfdee4"
      ]
    }
  ]
}
[/block]
### Go to API Keys section

Go to Settings -> API Keys.

Note the Public key and Secret Key.
Public Key is your identifier and can be shared. 
Secret Key is to not to be shared and kept secured.
These will be used to generate the API token required for [API authorization](api-authorization).

As for the "Webhook URL" and "Skip Commit Stage", these depend on your [deployment strategy](decide-on-deployment-strategy).


[block:image]
{
  "images": [
    {
      "image": [
        "https://files.readme.io/a1100d7-APIKeys.png",
        "APIKeys.png",
        1717,
        997,
        "#dfdee4"
      ]
    }
  ]
}
[/block]

[block:callout]
{
  "type": "info",
  "title": "Merchant Dashboard differ for sandbox and production environment",
  "body": "The merchant dashboard in sandbox and production environment are identical but different environment. You will be doing all your testing on sandbox and once ready, you will need to create account in production Merchant Dashboard and use the production API keys"
}
[/block]