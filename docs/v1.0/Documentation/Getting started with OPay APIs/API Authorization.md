---
title: "API Authorization"
excerpt: ""
---
## Overview
OPay uses [JWT](https://jwt.io/introduction/) to authenticate and authorize API calls.

All authenticated API endpoints require a JSON Web Token (JWT) encoded access token in the header.

## How to generate an Access token

Opay provides two endpoints:

* [`access/code`](https://opera-pay.readme.io/v1.0/reference#getaccesscode) : Takes merchant public key and generates an ephemeral access code which can be used to generate an Access token. 
* [`access/token`](https://opera-pay.readme.io/v1.0/reference#getaccesstoken) : Takes access code and private key to generate an JWT encoded Access token which can be used in the `Authorization` HTTP header 

A simple example of generating access tokens using curl is as below:
[block:code]
{
  "codes": [
    {
      "code": "PUBLIC_KEY=\"public-key\"\nPRIVATE_KEY=\"private-key\"\n\necho \"Calling access/code api\"\nRESPONSE=$(curl -s --request POST \\\n  --url https://bz-sandbox.opay-test.net/api/access/code \\\n  --data \"{\\\"publicKey\\\":\\\"$PUBLIC_KEY\\\"}\")\n\necho \"Parsing access/code response json to retrieve access code\"\nACCESS_CODE=$(echo $RESPONSE | jq \".data.accessCode\")\n\necho \"Access code :  $ACCESS_CODE\"\n\necho \"Calling access/token api\"\nRESPONSE=$(curl -s --request POST \\\n  --url https://bz-sandbox.opay-test.net/api/access/token \\\n  --data \"{\\\"privateKey\\\":\\\"$PRIVATE_KEY\\\",\\\"accessCode\\\":$ACCESS_CODE}\")\n\necho \"Parsing response json to retrieve access token\"\nACCESS_TOKEN=$(echo $RESPONSE | jq \".data.accessToken.value\")\necho \"Access token :  $ACCESS_TOKEN\"",
      "language": "shell"
    }
  ]
}
[/block]
## How to use the Access token to call Authenticated APIs
All the authenticated API endpoints require an Access token in the `Authorization` header as defined in the HTTP specification.

The format of the header is as follows:
[block:code]
{
  "codes": [
    {
      "code": "Authorization: Bearer <access-token>",
      "language": "http"
    }
  ]
}
[/block]