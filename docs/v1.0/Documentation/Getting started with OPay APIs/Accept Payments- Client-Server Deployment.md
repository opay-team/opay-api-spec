---
title: "Accept Payments: Client-Server Deployment"
excerpt: ""
---
## Accept Payments
Accepting payments is done via the Gateway APIs.
Depending on the payment verification mechanism (PIN, OTP, or 3Dsecure), the sequence will change. 

Here is a complete sequence diagram involving the following actors
* Client: Merchant-side client . This could be a mobile app or website. 
* Server: Merchant-side server which responds to webhook.
* OPay: OPay API endpoint. For testing, you should point it to sandboxed API endpoints.
* User: End user providing PIN/OTP/3Dsecure to verify the payment.
* Bank: This would be required only in case the payment instrument requires 3Dsecure verification. OPay would send the 3Dsecure URL to which the client needs the user to redirect to.
[block:image]
{
  "images": [
    {
      "image": [
        "https://files.readme.io/246db79-Accept_Payment__Client-Server_Deployment.png",
        "Accept Payment_ Client-Server Deployment.png",
        858,
        1057,
        "#f6f6f6"
      ]
    }
  ]
}
[/block]