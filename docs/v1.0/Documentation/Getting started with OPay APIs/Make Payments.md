---
title: "Make Payments"
excerpt: ""
---
## Make Payments
Opay's Order API allows merchants to enable end users to make payments for various utilities:
* Airtime
* Water 
* TV 
* Rent 

Apart from these, Order API can also be used to make payments from one OPay wallet to another. 

## Examples

### Airtime top up using wallet
[block:code]
{
  "codes": [
    {
      "code": "PUBLIC_KEY=\"public-key\"\nPRIVATE_KEY=\"private-key\"\n\necho \"1. Get access token to be able to call APIs\"\nACCESS_CODE=$(curl -s --request POST \\\n  --url https://bz-sandbox.opay-test.net/api/access/code \\\n  --data \"{\\\"publicKey\\\":\\\"$PUBLIC_KEY\\\"}\" | jq \".data.accessCode\")\n\nACCESS_TOKEN=$(curl -s --request POST \\\n  --url https://bz-sandbox.opay-test.net/api/access/token \\\n  --data \"{\\\"privateKey\\\":\\\"$PRIVATE_KEY\\\",\\\"accessCode\\\":$ACCESS_CODE}\" | jq -r \".data.accessToken.value\" )\n  \necho \"2. Call balance to check you have sufficient balance to be able to pay through wallet\"  \nRESPONSE=$(curl -s --request POST \\\n  --header 'Accept: application/json' \\\n  --header 'Content-type: application/json' \\\n  --header \"Authorization: token $ACCESS_TOKEN\" \\\n  --url https://bz-sandbox.opay-test.net/api/balance \\\n  --data '{\"countryCode\":\"NG\"}' )\n\nWALLET_BALANCE=$(echo $RESPONSE | jq -r '.data.walletBalance.value')\necho \"Balance in the wallet is $WALLET_BALANCE\"  \n\ncurl -s --request POST \\\n  --header 'Accept: application/json' \\\n  --header 'Content-type: application/json' \\\n  --header \"Authorization: token $ACCESS_TOKEN\" \\\n  --url https://bz-sandbox.opay-test.net/api/payment/order \\\n  --data '{\"orderConfig\":{\"serviceType\":\"airtime\",\"paymentAmount\":\"77\",\"instruments\":[{\"type\":\"coins\",\"countryCode\":\"NG\"}],\"recipientPhoneNumber\":\"2347081234123\",\"currencyISO\":\"NGN\"}}'",
      "language": "shell"
    }
  ]
}
[/block]
### Transfer money to another OPay wallet
[block:code]
{
  "codes": [
    {
      "code": "PUBLIC_KEY=\"public-key\"\nPRIVATE_KEY=\"private-key\"\n\necho \"1. Get access token to be able to call APIs\"\nACCESS_CODE=$(curl -s --request POST \\\n  --url https://bz-sandbox.opay-test.net/api/access/code \\\n  --data \"{\\\"publicKey\\\":\\\"$PUBLIC_KEY\\\"}\" | jq \".data.accessCode\")\n\nACCESS_TOKEN=$(curl -s --request POST \\\n  --url https://bz-sandbox.opay-test.net/api/access/token \\\n  --data \"{\\\"privateKey\\\":\\\"$PRIVATE_KEY\\\",\\\"accessCode\\\":$ACCESS_CODE}\" | jq -r \".data.accessToken.value\" )\n  \necho \"2. Call balance to check you have sufficient balance to be able to pay through wallet\"  \nRESPONSE=$(curl -s --request POST \\\n  --header 'Accept: application/json' \\\n  --header 'Content-type: application/json' \\\n  --header \"Authorization: token $ACCESS_TOKEN\" \\\n  --url https://bz-sandbox.opay-test.net/api/balance \\\n  --data '{\"countryCode\":\"NG\"}' )\n\nWALLET_BALANCE=$(echo $RESPONSE | jq -r '.data.walletBalance.value')\necho \"Balance in the wallet is $WALLET_BALANCE\"  \n\ncurl -s --request POST \\\n  --header 'Accept: application/json' \\\n  --header 'Content-type: application/json' \\\n  --header \"Authorization: token $ACCESS_TOKEN\" \\\n  --url https://bz-sandbox.opay-test.net/api/payment/order \\\n  --data '{\"orderConfig\":{\"serviceType\":\"coinsTransfer\",\"paymentAmount\":\"77\",\"instruments\":[{\"type\":\"coins\",\"countryCode\":\"NG\"}],\"recipientPhoneNumber\":\"2347081234123\",\"currencyISO\":\"NGN\"}}'",
      "language": "shell"
    }
  ]
}
[/block]