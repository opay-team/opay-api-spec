---
title: "Getting started with OPay Checkout"
excerpt: "How to add OPay as a checkout option to your website."
---
OPay Checkout offers a simple and secure interface for accepting payments on the web and mobile application. All the different scenarios and payment flows have been handled for you, making it the fastest and easiest way to accept payments.

5 Steps to set up OPay Checkout

Step 0: [Prerequisite](doc:prerequisite) 

Step 1: [Setup](doc:setup) 

Step 2: [Verify payment](doc:verify-payment) 

Step 3: [Setup webhook](doc:setup-webhook) 

Step 4: [Go live](doc:go-live) 
[block:image]
{
  "images": [
    {
      "image": [
        "https://files.readme.io/cf4b628-Screen_Shot_2018-06-29_at_8.06.23_AM.png",
        "Screen Shot 2018-06-29 at 8.06.23 AM.png",
        1022,
        928,
        "#8c908e"
      ]
    }
  ]
}
[/block]