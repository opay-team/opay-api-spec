---
title: "Go live"
excerpt: ""
---
At this point, you are almost ready to start accepting payments from real cards with OPay. To go live, please, toggle the Go Live button on the top-right section of the merchant dashboard as shown below:
[block:image]
{
  "images": [
    {
      "image": [
        "https://files.readme.io/e498a68-Screen_Shot_2018-06-29_at_10.17.07_AM.png",
        "Screen Shot 2018-06-29 at 10.17.07 AM.png",
        1498,
        120,
        "#c9c8d1"
      ]
    }
  ]
}
[/block]
Once that's done, you will be required to submit the documents below:

1. Details and CAC of your business

2. The BVN of the business owners.

:100: At this point, you will be live within 24 hours! If not, contact support@opay.team.