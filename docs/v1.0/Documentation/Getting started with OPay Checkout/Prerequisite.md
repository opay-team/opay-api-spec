---
title: "Prerequisite"
excerpt: ""
---
To get started with using OPay as a checkout option, you need to create a merchant account on our [dashboard](https://dashboard.operapay.com). Once your account is successfully created, it will be in __test mode__, which means all your integrations will use your [test cards](http://google.com) and [test bank accounts](http://google.com).

While the Integration is ongoing, there are several other things you need to make sure of:

**Technical**

1. Confirm that your server can include a TLSv1.2 connection to OPay's servers. More information about this requirement can be read here.

2. Join our Slack support workspace for immediate feedback on any issues you may encounter.

**KYC**

1. Prepare a CAC document of your business.

2. Get a BVN of the business owners.