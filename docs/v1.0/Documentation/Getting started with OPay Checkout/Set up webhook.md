---
title: "Set up webhook"
excerpt: ""
---
Every time a payment is completed, we always notify your server with the URL setup as the webhook URL. This is important because sometimes a payment status can only be known from the webhook event from our server.
[block:callout]
{
  "type": "info",
  "title": "Note",
  "body": "To set up a webhook URL, please go to Dashboard > Settings > API Keys & Webhook and paste your server's webhook URL into the input box."
}
[/block]
Below is a sample of the payload you receive from our webhook:

[block:code]
{
  "codes": [
    {
      "code": "{\n    \"payload\": {\n        \"status\": \"failed\",\n        \"status_description\": \"Unknown failure\",\n        \"reference\": \"3gciK7dX9yS8wLrmO8Qo\",\n        \"transaction_id\": \"5b2f7a4da4ded50001fcecec\",\n        \"instrument_id\": \"5b2f7a4da4ded50001fcecec\"\n    },\n    \"type\": \"transaction-status\"\n}",
      "language": "json"
    }
  ]
}
[/block]

**Configuration Options**
[block:parameters]
{
  "data": {
    "h-0": "Options",
    "h-1": "Descriptions",
    "0-0": "instrument_id",
    "1-0": "status_description",
    "2-0": "reference",
    "3-0": "transaction_id",
    "4-0": "status",
    "4-1": "This is the status of the payment. It can either be __successful__, __failed__ or __pending__. When in pending state, it means that the payment is not complete and you should wait a couple of seconds to reconfirm by using the verify transaction approach.",
    "2-1": "This is the unique reference you send to us in the setup or when you initiated the payment on your platform.",
    "1-1": "A description message of the payment.",
    "0-1": "This is a unique ID that represents the card when provided. It means you can charge that same card again without any authentication required from the card owner.",
    "3-1": "This is a reference from our system for identifying the payment."
  },
  "cols": 2,
  "rows": 5
}
[/block]