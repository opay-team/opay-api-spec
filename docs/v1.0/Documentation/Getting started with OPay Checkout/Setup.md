---
title: "Setup"
excerpt: ""
---
[block:callout]
{
  "type": "info",
  "title": "Note",
  "body": "Please note that the key to use is your public-key, which can be found in your Dashboard > Settings > API Keys & Webhooks"
}
[/block]
Below is sample code on how to setup OPay Checkout for one-time and recurring payments. To find out more about recurring payments, visit [this tutorial](http://google.com).

[block:code]
{
  "codes": [
    {
      "code": "<script src=\"https://bz-sandbox.opay-test.net/static/opay-cherry.js\"></script>\n<script>\n    window.onload = function() {\n        var ngContext = new OpayPaymentContext(\n          \"#opay-embed-ng\", //id of payment button\n          \"22\", //amount to charge\n          \"NGN\", //currency. KE is also supported for Kenya Shillings\n          \"NG\" //country code\n        )\n        ngContext.reference = \"merchant-ng-reference\";\n        ngContext.customerEmail = \"email@customer.com\";\n        ngContext.customerPhone = \"700000001\";\n        ngContext.callbackURL = \"https://merchant.com/callback\";\n  \t\t\tngContext.logo = \"YOUR_LOGO_HERE\";\n  \n        ngContext.successCallback = function successCallback(ctxt) {\n            document.querySelector(ctxt.selector + \" + h2\").innerHTML = \"Successful transaction!\";\n        }\n        ngContext.failureCallback = function failureCallback(ctxt) {\n            document.querySelector(ctxt.selector + \" + h2\").innerHTML = \"Failed transaction!\";\n        }\n\n        // transaction should be initialized when form exists on page\n        var txNG = new OpayTransaction(\"public-key\", ngContext);\n    }\n</script>\n\n<!-- this enclosing form is required for committing the transaction (see Step 2) -->\n<form id=\"opay-embed-ng\" action=\"/demo/commit\" style=\"padding: 10px 0\">\n</form>",
      "language": "javascript",
      "name": "OPay Checkout"
    },
    {
      "code": "",
      "language": "text",
      "name": "OPay Checkout Recurring Payment"
    }
  ]
}
[/block]
Once your payment is complete, we will redirect to your set __callbackURL__, if it is set. However, if it is not set, then we will attempt to call the __successCallback__ function if the payment is complete and successful or the __failureCallback__ function if the payment is complete but failed. Once either of these are done, please make sure to [Verify payment](doc:verify-payment) at all times before proceeding to give values. 

**Configuration Options** 

[block:parameters]
{
  "data": {
    "h-0": "Options",
    "h-1": "Descriptions",
    "0-0": "reference*",
    "1-0": "customerPhone",
    "2-0": "customerEmail",
    "3-0": "successCallback",
    "4-0": "failureCallback",
    "5-0": "callbackURL",
    "6-0": "logo"
  },
  "cols": 2,
  "rows": 7
}
[/block]