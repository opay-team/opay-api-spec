#!/usr/bin/env node
'use strict';
var Path = require('path');

require('shelljs/global');
set('-e');

mkdir('-p', 'web_deploy')

cp('-R', 'web/*', 'web_deploy/');

exec('npm run swagger bundle -- -b authorization/        -o web_deploy/authorization.json');
exec('npm run swagger bundle -- -b authorization/ --yaml -o web_deploy/authorization.yaml');
exec('npm run swagger bundle -- -b order/                -o web_deploy/order.json');
exec('npm run swagger bundle -- -b order/         --yaml -o web_deploy/order.yaml');
exec('npm run swagger bundle -- -b gateway/        -o web_deploy/gateway.json');
exec('npm run swagger bundle -- -b gateway/ --yaml -o web_deploy/gateway.yaml');
exec('npm run swagger bundle -- -b remittance/        -o web_deploy/remittance.json');
exec('npm run swagger bundle -- -b remittance/ --yaml -o web_deploy/remittance.yaml');
exec('npm run swagger bundle -- -b users/        -o web_deploy/users.json');
exec('npm run swagger bundle -- -b users/ --yaml -o web_deploy/users.yaml');

var SWAGGER_UI_DIST = Path.dirname(require.resolve('swagger-ui'));
rm('-rf', 'web_deploy/swagger-ui/')
cp('-R', SWAGGER_UI_DIST, 'web_deploy/swagger-ui/')
sed('-i', 'http://petstore.swagger.io/v2/swagger.json', '../swagger.json', 'web_deploy/swagger-ui/index.html')

